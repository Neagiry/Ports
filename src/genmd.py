#!/usr/bin/python3

import os
import toml

PORTS_DIR = "../ports"
METADATA_FILE = f"{PORTS_DIR}/metadata.toml"
PORTS_ARCHIVE = "../ports.txz"

md_data = toml.load(METADATA_FILE)
cat_list = md_data['port_sys']['categories']
ports_list = []

def check_port(cat: str, name: str) -> bool:
    content = os.listdir(f"{PORTS_DIR}/{cat}/{name}")
    return 'install' in content and 'port.toml' in content

for cat in cat_list:
    content = os.listdir(f"{PORTS_DIR}/{cat}")
    for file in content:
        if os.path.isdir(f"{PORTS_DIR}/{cat}/{file}") and check_port(cat, file):
            ports_list.append(f"{cat}/{file}")

ports_list.sort()

md_data['port_sys']['ports'] = ports_list

with open(METADATA_FILE, 'w') as f:
    toml.dump(md_data, f)
