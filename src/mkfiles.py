#!/usr/bin/python3
#
# Copyright (C) 2022 Michail Krasnov <linuxoid85@gmail.com>

import os
import sys

def get_files(rpath: str) -> list:
    flist = []
    for root, dirs, files in os.walk(rpath):
        for name in files:
            flist.append(os.path.join(root, name))
    return flist

try:
    arg = sys.argv[1]
except IndexError:
    exit(1)

FILE = "./files.list"

if os.path.exists(FILE):
    os.remove(FILE)

with open(FILE, 'a') as f:
    files = get_files(arg)
    for file in files:
        f.write(f"{file}\n")
