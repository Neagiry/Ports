# wayland-protocols-1.26

Additional Wayland protocols that add functionality outside of protocols already in the Wayland core

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 6.9 Mb

### Dependencies

- **required:**  wayland/wayland
- **recommend:** none
- **optional:** none
- **conflict:** none

