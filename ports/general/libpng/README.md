# libpng-1.6.37

Libraries used by other programs for reading and writing PNG files. The PNG format was designed as a replacement for GIF and to a lesser extent TIFF with many improvements and extensions and lack of patent problems

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 14 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** none
- **conflict:** none

