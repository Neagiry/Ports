# wl-clipboard-2.1.0

Command-line copy/paste utilities for Wayland

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1.4 Mb

### Dependencies

- **required:** wayland/wayland wayland/wayland-protocols
- **recommend:** none
- **optional:** none
- **conflict:** none

