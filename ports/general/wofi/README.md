# wofi-1.2.4

Wofi is a launcher/menu program for wlroots based wayland compositors such as sway

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2 Mb

### Dependencies

- **required:** base/pkg-config base/meson general/libgtk-3 wayland/wayland
- **recommend:** none
- **optional:** none
- **conflict:** none

