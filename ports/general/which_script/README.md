# which_script-0.1

shows the full path of (shell) commands installed in your PATH

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 0.1 Mb

### Dependencies

- **required:**  base/bash
- **recommend:** none
- **optional:** none
- **conflict:** none

