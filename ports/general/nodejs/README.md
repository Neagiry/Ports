# nodejs-v16.18.0

Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v1.2a3'
- **usage:** 750.0 Mb

### Dependencies

- **required:**  general/which_script
- **recommend:**  basicnet/c-ares general/icu general/libuv basicnet/nghttp2
- **optional:** none
- **conflict:** none

