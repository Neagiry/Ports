# llvm-15.0.1

The LLVM package contains a collection of modular and reusable compiler and toolchain technologies

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 3891.2 Mb

### Dependencies

- **required:**  base/cmake
- **recommend:** none
- **optional:** general/doxygen general/git general/graphviz general/libxml2 basicnet/rsync general/valgrind
- **conflict:** none

