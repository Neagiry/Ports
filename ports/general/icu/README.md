# icu-70.1

The ICU is a mature widely used set of C/C++ libraries providing Unicode and Globalization support for software applications. ICU is widely portable and gives applications the same results on all platforms.

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 367 Mb

### Dependencies

- **required:**  
- **recommend:**  general/llvm
- **optional:**  
- **conflict:**  

