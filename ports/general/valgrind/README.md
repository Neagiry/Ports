# valgrind-3.19.0

Valgrind is an instrumentation framework for building dynamic analysis tools.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 276 Mb

### Dependencies

- **required:**  base/sed general/llvm
- **recommend:** none
- **optional:** none
- **conflict:** none

