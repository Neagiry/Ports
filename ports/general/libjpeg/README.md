# libjpeg-2.1.2

libjpeg-turbo is a fork of the original IJG libjpeg which uses SIMD to accelerate baseline JPEG compression and decompression. libjpeg is a library that implements JPEG image encoding decoding and transcoding

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 33 Mb

### Dependencies

- **required:**  base/cmake general/nasm
- **recommend:** none
- **optional:** none
- **conflict:** none

