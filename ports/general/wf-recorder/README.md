# wf-recorder-0.3.0

wf-recorder is a utility program for screen recording of wlroots-based compositors

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1 Mb

### Dependencies

- **required:** wayland/wayland wayland/wayland-protocols general/ffmpeg
- **recommend:** none
- **optional:** none
- **conflict:** none

