# cpio-2.13

Tools for archiving

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 17.0 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:**  pst/texlive
- **conflict:** none

