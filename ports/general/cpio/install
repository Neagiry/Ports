#!/bin/bash -e
# Build script for 'cpio' package
# Copyright (C) 2021, 2022 Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>

. /usr/share/cport/shared.sh

NAME="cpio"
VERSION="2.13"
DOC="no"

cd /usr/src/"$NAME-$VERSION"

sed -i '/The name/,+2 d' src/global.c

./configure --prefix=/usr \
            --bindir=/bin \
            --enable-mt   \
            --with-rmt=/usr/libexec/rmt

make

makeinfo --html            -o doc/html      doc/cpio.texi
makeinfo --html --no-split -o doc/cpio.html doc/cpio.texi
makeinfo --plaintext       -o doc/cpio.txt  doc/cpio.texi

if cport check --is-installed pst/texlive >> /dev/null; then
    make -C doc pdf
    make -C doc ps
    DOC="yes"
fi

make install

install -v -m755 -d /usr/share/doc/cpio-$VERSION/html
install -v -m644    doc/html/* \
                    /usr/share/doc/cpio-$VERSION/html
install -v -m644    doc/cpio.{html,txt} \
                    /usr/share/doc/cpio-$VERSION

if [ $DOC -eq "yes" ]; then
    install -v -m644 doc/cpio.{pdf,ps,dvi} \
                 /usr/share/doc/cpio-$VERSION
fi
