# libxslt-1.1.36

The libxslt package contains XSLT libraries used for extending libxml2 libraries to support XSLT files

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 35 Mb

### Dependencies

- **required:**   general/libxml2 
- **recommend:**   pst/docbook-xml pst/docbook-xsl 
- **optional:** none
- **conflict:** none

