# glib-2.72.3

Low-level libraries useful for providing data structure handling for C portability wrappers and interfaces for such runtime functionality as an event loop threads dynamic loading and an object system

## Detailed information
### Port/package

- **maintainer:** Repyakh Ivan <red-122@mail.ru>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 84.5 Mb

### Dependencies

- **required:**  base/meson base/ninja base/gcc
- **recommend:**  general/libxlst general/pcre
- **optional:** general/dbus pst/docbook pst/docbook-xsl general/gtk-doc
- **conflict:** none

