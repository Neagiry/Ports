# docbook-xml-4.5-4.5

The DocBook-4.5 XML DTD-4.5 package contains document type definitions for verification of XML data files against the DocBook rule set

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1.2 Mb

### Dependencies

- **required:** general/libxml2 general/unzip pst/sgml-common
- **recommend:** none
- **optional:** none
- **conflict:** none

