# ntfs-3g-2021.8.22

The Ntfs-3g package contains a stable read-write open source driver for NTFS partitions. NTFS partitions are used by most Microsoft operating systems. Ntfs-3g allows you to mount NTFS partitions in read-write mode from your Linux system. It uses the FUSE kernel module to be able to implement NTFS support in user space. The package also contains various utilities useful for manipulating NTFS partitions.

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 22 Mb

### Dependencies

- **required:**  
- **recommend:**  
- **optional:**  
- **conflict:**  

