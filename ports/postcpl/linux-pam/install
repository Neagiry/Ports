#!/bin/bash -e
# Build script for 'linux-pam' package
# Copyright (C) 2021, 2022 Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>

NAME="Linux-PAM"
VERSION="1.5.2"

cd /usr/src/"$NAME-$VERSION"

function print_shadow_warning() {
    echo -e "\a\e[1m--== NOTE ==--\e[0m"
    echo -e "\a\a\a"
    echo "'base/shadow' port needs to be rebuilded after installing and
configuring Linux PAM.

With Linux-PAM-$VERSION and higher, the pam_cracklib module is not installed by
default. To enforce strong passwords, it is recommended to use
libpwquality-1.4.4."
    sleep 2
    echo -e "\a"
    sleep 2
}

print_shadow_warning

echo -e "\a\e[1;31mReinstallation or upgrade of Linux PAM\e[0m"
echo "If you have a system with Linux PAM installed and working, be careful when
modifying the files in /etc/pam.d, since your system may become totally
unusable. If you want to run the tests, you do not need to create another
/etc/pam.d/other file. The installed one can be used for that purpose.

You should also be aware that make install overwrites the configuration files in
/etc/security as well as /etc/environment. In case you have modified those
files, be sure to back them up."
sleep 2
echo -e "\a\e[1;31mStarting build system...\e[0m"
sleep 2

sed -e /service_DATA/d \
    -i modules/pam_namespace/Makefile.am &&
autoreconf

./configure --prefix=/usr                        \
            --sbindir=/sbin                      \
            --sysconfdir=/etc                    \
            --libdir=/usr/lib                    \
            --enable-securedir=/usr/lib/security \
            --docdir=/usr/share/doc/Linux-PAM-$VERSION

make

if [ ! -d "/etc/pam.d" ]; then
install -v -m755 -d /etc/pam.d
cat > /etc/pam.d/other << "EOF"
auth     required       pam_deny.so
account  required       pam_deny.so
password required       pam_deny.so
session  required       pam_deny.so
EOF
fi

echo -e -n "\a\e[1;31mThis is a first installation (y/n)? \e[0m"
read run

if [ $run == "y" ]; then
    rm -fv /etc/pam.d/other
fi

make install
chmod -v 4755 /sbin/unix_chkpwd

for file in pam pam_misc pamc; do
  mv -v /usr/lib/lib${file}.so.* /lib
  ln -sfv ../../lib/$(readlink /usr/lib/lib${file}.so) /usr/lib/lib${file}.so
done

echo -e -n "\a\e[1;31mThis is a first installation (y/n)? \e[0m"
read run

if [ $run != "y" ]; then
    echo -e "\a\e[1;32mInstallation complete\e[0m\a\n"
    exit 0
fi

install -vdm755 /etc/pam.d &&
cat > /etc/pam.d/system-account << "EOF" &&
# Begin /etc/pam.d/system-account

account   required    pam_unix.so

# End /etc/pam.d/system-account
EOF

cat > /etc/pam.d/system-auth << "EOF" &&
# Begin /etc/pam.d/system-auth

auth      required    pam_unix.so

# End /etc/pam.d/system-auth
EOF

cat > /etc/pam.d/system-session << "EOF"
# Begin /etc/pam.d/system-session

session   required    pam_unix.so

# End /etc/pam.d/system-session
EOF
cat > /etc/pam.d/system-password << "EOF"
# Begin /etc/pam.d/system-password

# use sha512 hash for encryption, use shadow, and try to use any previously
# defined authentication token (chosen password) set by any prior module
password  required    pam_unix.so       sha512 shadow try_first_pass

# End /etc/pam.d/system-password
EOF

cat > /etc/pam.d/other << "EOF"
# Begin /etc/pam.d/other

auth        required        pam_warn.so
auth        required        pam_deny.so
account     required        pam_warn.so
account     required        pam_deny.so
password    required        pam_warn.so
password    required        pam_deny.so
session     required        pam_warn.so
session     required        pam_deny.so

# End /etc/pam.d/other
EOF

print_shadow_warning
