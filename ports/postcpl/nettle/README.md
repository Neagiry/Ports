# nettle-3.8.1

Low-level cryptographic library that is designed to fit easily in many contexts

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 90 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:**  general/valgring
- **conflict:** none

