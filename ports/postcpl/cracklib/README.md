# cracklib-2.9.7

A library used to enforce strong passwords by comparing user selected passwords to words in chosen word lists

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 4.2 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** none
- **conflict:** none

