#!/bin/bash -e
# Build script for 'gcc-p2' package
# Copyright (C) 2021, 2022 Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>

. /usr/share/cport/shared.sh

NAME="gcc"
VERSION="12.2.0"

cd /mnt/calm/sources

wget https://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz \
  https://ftp.gnu.org/gnu/mpfr/mpfr-4.1.0.tar.xz \
  https://ftp.gnu.org/gnu/gmp/gmp-6.2.1.tar.xz

cd /mnt/calm/sources/"$NAME-$VERSION"

tar -xvf ../mpfr-4.1.0.tar.xz
mv -v mpfr-4.1.0 mpfr
tar -xvf ../gmp-6.2.1.tar.xz
mv -v gmp-6.2.1 gmp
tar -xvf ../mpc-1.2.1.tar.gz
mv -v mpc-1.2.1 mpc

case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
  ;;
esac

sed '/thread_header =/s/@.*@/gthr-posix.h/' \
    -i libgcc/Makefile.in libstdc++-v3/include/Makefile.in

mkdir -v build
cd build

../configure                                       \
    --build=$(../config.guess)                     \
    --host=$CALM_TGT                                \
    --target=$CALM_TGT                              \
    LDFLAGS_FOR_TARGET=-L$PWD/$CALM_TGT/libgcc      \
    --prefix=/usr                                  \
    --with-build-sysroot=$CALM                      \
    --enable-default-pie                           \
    --enable-default-ssp                           \
    --disable-nls                                  \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --enable-languages=c,c++

make
make DESTDIR=$CALM install

ln -sv gcc $CALM/usr/bin/cc
