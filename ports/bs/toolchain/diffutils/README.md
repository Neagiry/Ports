# diffutils-3.8

Programs that show the differences between files or directories

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 26.0 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** none
- **conflict:** none

