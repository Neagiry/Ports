# findutils-4.9.0

Programs to find files. These programs are provided to recursively search through a directory tree and to create maintain and search a database (often faster than the recursive find but is unreliable if the database has not been recently updated)

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 42.2 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** none
- **conflict:** none

