# xwayland-22.1.0

Xorg server running on top of the wayland server. It has been separated from the main Xorg server package. It allows running X clients inside a wayland session

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 65 Mb

### Dependencies

- **required:** xorg/x11-libs/libxcvt general/pixman wayland/wayland-protocols xorg/x11-minimal/xfonts
- **recommend:**  xorg/x11-minimal/mesa
- **optional:**  general/git xorg/x11-minimal/xlegacy
- **conflict:** none

