# xcb-util-cursor-0.1.3

Module that implements the XCB cursor library. It is the XCB replacement for libXcursor

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2.5 Mb

### Dependencies

- **required:**  xorg/x11-minimal/xcb-util
- **recommend:** none
- **optional:**  general/doxygen
- **conflict:** none

