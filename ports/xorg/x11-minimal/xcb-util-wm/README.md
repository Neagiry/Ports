# xcb-util-wm-0.4.1

Libraries which provide client and window-manager helpers for EWMH and ICCCM

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 3.3 Mb

### Dependencies

- **required:**  xorg/x11-libs/libxcb
- **recommend:** none
- **optional:**  general/doxygen
- **conflict:** none

