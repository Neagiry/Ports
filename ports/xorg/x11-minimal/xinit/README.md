# xinit-1.4.1

Usable script to start the xserver

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 1.5 Mb

### Dependencies

- **required:**  xorg/x11-libs/xlibs
- **recommend:** xorg/wm/twm xorg/x11-minimal/xclock xorg/x11-minimal/xterm
- **optional:** none
- **conflict:** none

