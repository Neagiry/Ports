# xclock-1.0.9

Simple clock application which is used in the default xinit configuration

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 0.172 Mb

### Dependencies

- **required:**  xorg/x11-libs/xlibs
- **recommend:** none
- **optional:** none
- **conflict:** none

