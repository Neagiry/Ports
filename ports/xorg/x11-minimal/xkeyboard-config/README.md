# xkeyboard-config-2.35.1

Keyboard configuration database for the X Window System

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 14 Mb

### Dependencies

- **required:**  xorg/x11-libs/xlibs
- **recommend:** none
- **optional:** none
- **conflict:** none

