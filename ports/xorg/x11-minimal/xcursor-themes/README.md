# xcursor-themes-1.0.6

Redglass and whiteglass animated cursor themes

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 15 Mb

### Dependencies

- **required:**  xorg/x11-minimal/xapps
- **recommend:** none
- **optional:** none
- **conflict:** none

