# xorg-server-21.1.3

Core of the X Window system

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 175 Mb

### Dependencies

- **required:** xorg/x11-libs/libxcvt general/pixman xorg/x11-minimal/xfonts xorg/x11-minimal/xkeyboard-config xorg/x11-libs/libepoxy
- **recommend:** general/elogind net/libtirpc
- **optional:** general/doxygen xorg/x11-minimal/xcb-util-keysyms xorg/x11-minimal/xcb-util-image xorg/x11-minimal/xcb-util-renderutil xorg/x11-minimal/xcb-util-wm pst/xmlto
- **conflict:** none

