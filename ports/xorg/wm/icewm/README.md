# icewm-2.9.9

IceWM is a window manager with the goals of speed simplicity and not getting in the user's way

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 54.0 Mb

### Dependencies

- **required:** base/cmake xorg/x11-libs/gdk-pixbuf-xlib general/pymo/markdown
- **recommend:**  general/librsvg
- **optional:** general/pymo/asciidoc general/fribidi xorg/x11-libs/imlib2 media/libao media/libsndfile media/alsa-lib
- **conflict:** none

