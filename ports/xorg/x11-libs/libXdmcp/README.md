# libXdmcp-1.1.3

Library implementing the X Display Manager Control Protocol

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2.8 Mb

### Dependencies

- **required:**  xorg/x11-minimal/xorgproto
- **recommend:** none
- **optional:** none
- **conflict:** none

