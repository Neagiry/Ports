# libxcb-1.14

Interface to the X Window System protocol which replaces the current Xlib interface. Xlib can also use XCB as a transport layer allowing software to make requests and receive responses with both.

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 27 Mb

### Dependencies

- **required:**  xorg/x11-libs/libXau xorg/x11-minimal/xcb-proto
- **recommend:**  xorg/x11-libs/libXdmcp
- **optional:**  general/doxygen general/libxslt
- **conflict:** none

