# xlibs-0.1

The Xorg libraries provide library routines that are used within all X Window applications

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 224 Mb

### Dependencies

- **required:**  general/fontconfig general/freetype xorg/x11-libs/libxcb
- **recommend:**  general/elogind
- **optional:**  pst/xmlto net/links
- **conflict:** none

