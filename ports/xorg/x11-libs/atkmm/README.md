# atkmm-2.28.3

Atkmm is the official C++ interface for the ATK accessibility toolkit library

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 11 Mb

### Dependencies

- **required:**  xorg/x11-libs/atk general/glibmm
- **recommend:** none
- **optional:** none
- **conflict:** none

