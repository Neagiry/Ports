# at-spi2-atk-2.38.0

At-Spi2 Atk package contains a library that bridges ATK to At-Spi2 D-Bus service

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 8.5 Mb

### Dependencies

- **required:**  xorg/x11-libs/at-spi2-core xorg/x11-libs/atk
- **recommend:** none
- **optional:** none
- **conflict:** none

