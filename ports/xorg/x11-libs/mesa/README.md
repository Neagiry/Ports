# mesa-22.2.0

An OpenGL compatible 3D graphics library

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 366.0 Mb

### Dependencies

- **required:** xorg/x11-libs/xlibs xorg/x11-libs/libdrm xorg/x11-drivers/libva xorg/x11-drivers/libvdpau general/pymo/mako general/llvm
- **recommend:** none
- **optional:** none
- **conflict:** none

