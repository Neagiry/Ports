# atk-2.38.0

ATK provides the set of accessibility interfaces that are implemented by other toolkits and applications. Using the ATK interfaces accessibility tools have full access to view and control running applications

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 8.6 Mb

### Dependencies

- **required:**  general/glib
- **recommend:**  general/gobject-introspection
- **optional:**  general/gtk-doc
- **conflict:** none

