# libvdpau-1.5

A library which implements the VDPAU library

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 4.6 Mb

### Dependencies

- **required:**  xorg/x11-libs/xlibs
- **recommend:** none
- **optional:**  general/doxygen general/graphviz general/mesa
- **conflict:** none

