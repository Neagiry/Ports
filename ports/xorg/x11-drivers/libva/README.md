# libva-2.15.0

The libva package contains a library which provides access to hardware accelerated video processing using hardware to accelerate video processing in order to offload the central processing unit (CPU) to decode and encode compressed digital video. The VA API video decode/encode interface is platform and window system independent targeted at Direct Rendering Infrastructure (DRI) in the X Window System however it can potentially also be used with direct framebuffer and graphics sub-systems for video output. Accelerated processing includes support for video decoding video encoding subpicture blending and rendering

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 7.8 Mb

### Dependencies

- **required:**  xorg/x11-libs/libdrm
- **recommend:**  general/mesa
- **optional:**  general/doxygen wayland/wayland
- **conflict:** none

