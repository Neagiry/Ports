# gvim-8.2.4383

A vi clone with extra features as compared to the original vi.

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0'
- **usage:** 112 Mb

### Dependencies

- **required:**  xorg/x11-libs/gtk3
- **recommend:** none
- **optional:** general/gpm general/lua basicnet/rsync general/ruby
- **conflict:** none

