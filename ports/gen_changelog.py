#!/usr/bin/python3

import os
import json

ch_dir = "../changelogs/"
ch_content = os.listdir(ch_dir)

json_data: list = []

for file in ch_content:
    with open(f"{ch_dir}/{file}") as f:
        _data = json.load(f)
    json_data.append(_data)

with open("../changelog.json", "w") as f:
    json.dump(json_data, f)
