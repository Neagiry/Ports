# diffutils-3.8

The Diffutils package contains programs that show the differences between files or directories.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 28 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gawk base/gcc base/gettext base/glibc base/grep base/make base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

