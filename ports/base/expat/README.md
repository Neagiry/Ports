# expat-2.4.8

The Expat package contains a stream oriented C library for parsing XML.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 8 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gawk base/gcc base/glibc base/grep base/make base/sed base/python base/xml::parser
- **recommend:** none
- **optional:** none
- **conflict:** none

