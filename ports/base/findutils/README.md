# findutils-4.9.0

The Findutils package contains programs to find files.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 41 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/gettext base/glibc base/grep base/make base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

