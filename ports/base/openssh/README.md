# openssh-9.1p1

SSH protocol implementation for remote login command execution and file transfer

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 53 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:** postcpl/linux-pam net/krb5 base/openssl general/libedit net/net-tools general/sysstat
- **conflict:** none

