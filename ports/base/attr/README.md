# attr-2.5.1

Commands for Manipulating Filesystem Extended Attributes.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 4.4 Mb

### Dependencies

- **required:**  base/libtool
- **recommend:** none
- **optional:** none
- **conflict:** none

