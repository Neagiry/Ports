# libelf-0.187

Libelf is a library for handling ELF (Executable and Linkable Format) files.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 106 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/make base/zlib
- **recommend:** none
- **optional:** none
- **conflict:** none

