# libpipeline-1.5.6

The Libpipeline package contains a library for manipulating pipelines of subprocesses in a flexible and convenient way

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 98 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/gawk base/gcc base/glibc base/grep base/make base/sed base/texinfo base/man-db
- **recommend:** none
- **optional:** none
- **conflict:** none

