# mpfr-4.1.0

The MPFR package contains functions for multiple precision math

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 33 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/gawk base/gcc base/glibc base/grep base/make base/sed base/texinfo base/gmp
- **recommend:** none
- **optional:** none
- **conflict:** none

