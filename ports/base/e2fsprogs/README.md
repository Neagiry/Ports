# e2fsprogs-1.46.5

The e2fsprogs package contains the utilities for handling the ext2 file system. It also supports the ext3 and ext4 journaling file systems.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 11 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/gawk base/gcc base/glibc base/grep base/gzip base/make base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

