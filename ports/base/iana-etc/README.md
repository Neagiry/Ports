# iana-etc-20220610

Data for network services and protocols

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 4.7 Mb

### Dependencies

- **required:**  base/coreutils base/perl
- **recommend:** none
- **optional:** none
- **conflict:** none

