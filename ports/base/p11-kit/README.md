# p11-kit-0.24.1

PKG provided a way to load and enumerate PKCS #11 modules.

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v1.2a2' 'v1.2a3'
- **usage:** 44.0 Mb

### Dependencies

- **required:** none
- **recommend:**  base/libtasn mase/make-ca
- **optional:**  general/gtk-doc general-lib/libxlst postcpl/nss
- **conflict:** none

