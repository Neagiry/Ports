# libarchive-3.6.1

Single interface for reading/writing various compression formats

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 4.1 Mb

### Dependencies

- **required:** none
- **recommend:** none
- **optional:**  general/libxml2 postcpl/nettle general/lzo
- **conflict:** none

