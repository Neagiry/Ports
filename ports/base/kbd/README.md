# kbd-2.5.1

The Kbd package contains key-table files console fonts and keyboard utilities

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 37 Mb

### Dependencies

- **required:** base/bash base/binutils base/bison base/check base/coreutils base/flex base/gcc base/glibc base/gettext base/make base/patch base/gzip base/sed
- **recommend:** none
- **optional:** none
- **conflict:** none

