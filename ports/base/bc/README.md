# bc-5.3.3

The bc utility shall implement an arbitrary precision calculator.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 7.7 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gawk base/gcc base/glibc base/grep base/make base/ncurses base/readline
- **recommend:** none
- **optional:** none
- **conflict:** none

