# gawk-5.1.1

The Gawk package contains programs for manipulating text files.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 49 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/gcc base/glibc base/grep base/gettext base/gmp base/make base/mpfr base/patch base/readline base/sed base/texinfo
- **recommend:** none
- **optional:**  base/libsigsegv
- **conflict:** none

