# acl-2.3.1

Base pkg

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 6.1 Mb

### Dependencies

- **required:** base/attr base/bash base/binutils base/coreutils base/gcc base/gettext base/grep base/m4 base/make base/perl base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

