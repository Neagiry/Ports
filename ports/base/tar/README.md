# tar-1.34

The Tar package provides the ability to create tar archives as well as perform various other kinds of archive manipulation

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 48 Mb

### Dependencies

- **required:** base/acl base/attr base/bash base/binutils base/bison base/coreutils base/gcc base/gettext base/glibc base/grep base/inetutils base/make base/sed base/texinfo
- **recommend:** base/bzip2 base/gzip base/xz
- **optional:** none
- **conflict:** none

