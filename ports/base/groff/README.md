# groff-1.22.4

The Groff package contains programs for processing and formatting text.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 97 Mb

### Dependencies

- **required:** base/bash base/binutils base/bison base/coreutils base/gawk base/gcc base/glibc base/grep base/make base/patch base/sed base/texinfo base/perl base/man-db
- **recommend:** none
- **optional:**  general/uchardet postcpl/ghostscript
- **conflict:** none

