# file-5.42

The File package contains a utility for determining the type of a given file or files.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 25 Mb

### Dependencies

- **required:** base/bash base/binutils base/bzip2 base/coreutils base/diffutils base/gawk base/gcc base/glibc base/grep base/make base/patch base/sed base/xz base/zlib
- **recommend:** none
- **optional:** none
- **conflict:** none

