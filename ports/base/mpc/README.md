# mpc-1.2.1

The MPC package contains a library for the arithmetic of complex numbers with arbitrarily high precision and correct rounding of the result

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 10 Mb

### Dependencies

- **required:** base/bash base/binutils base/coreutils base/diffutils base/gawk base/gcc base/glibc base/grep base/gmp base/make base/mpfr base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

