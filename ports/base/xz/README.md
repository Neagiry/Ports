# xz-5.2.5

XZ Utils is free general-purpose data compression software with a high compression ratio.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 20 Mb

### Dependencies

- **required:** base/bash base/gcc base/make base/mkdir base/sed base/glibc
- **recommend:**  base/mktemp
- **optional:**  base/gzip base/bzip2 base/lzop
- **conflict:** none

