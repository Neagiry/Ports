# grub-2.06

The GRUB package contains the GRand Unified Bootloader

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 172 Mb

### Dependencies

- **required:** base/bash base/binutils base/bison base/coreutils base/diffutils base/gcc base/glibc base/grep base/ncurses base/make base/sed base/texinfo base/gettext base/xz
- **recommend:** none
- **optional:** none
- **conflict:** none

