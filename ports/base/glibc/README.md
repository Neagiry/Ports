# glibc-2.36

Master C library. This library provides the basic routines for allocating memory searching directories opening and closing files reading and writing files string handling pattern matching arithmetic and so on

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 2867.2 Mb

### Dependencies

- **required:** base/bash base/binutils base/bison base/coreutils base/diffutils base/gawk base/gcc base/gettext base/grep base/gzip base/linux base/make base/perl base/python base/sed base/texinfo
- **recommend:** none
- **optional:**  base/file
- **conflict:** none

