# automake-1.16.5

GNU Automake is a tool for automatically generating Makefile.in files compliant with the GNU Coding Standards.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 18 Mb

### Dependencies

- **required:** base/autoconf base/bash base/coreutils base/gettext base/grep base/m4 base/make base/perl base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

