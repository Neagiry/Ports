# bash-5.1.16

Bash is the GNU Project's shell—the Bourne Again SHell.

## Detailed information
### Port/package

- **maintainer:** Sergey Gaberer <nordic.dev@pm.me>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 75 Mb

### Dependencies

- **required:** base/bash base/binutils base/bison base/coreutils base/diffutils base/gawk base/gcc base/glibc base/grep base/ncurses base/make base/patch base/readline base/sed base/texinfo
- **recommend:** none
- **optional:** none
- **conflict:** none

