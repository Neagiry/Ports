# sysklogd-1.5.1

Programs for logging system messages such as those given by the kernel when unusual things happen

## Detailed information
### Port/package

- **maintainer:** Michail Krasnov ⚪️🔵️⚪️ <linuxoid85@gmail.com>
- **releases:**  'v2.0a1' 'v2.0a2' 'v2.0a3'
- **usage:** 0.6 Mb

### Dependencies

- **required:** base/binutils base/coreutils base/gcc base/glibc base/make base/patch
- **recommend:** none
- **optional:** none
- **conflict:** none

